package ru.boltunov.model;

import java.util.*;

public class Provider {
	private UUID id;
	private String name;
	private String address;
	private String phone;
	
	public Provider() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Provider(String name, String address, String phone) {
		super();
		// TODO Auto-generated constructor stub
		this.id = UUID.randomUUID();
		this.name = name;
		this.address = address;
		this.phone = phone;
	}
	
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
		
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
}
