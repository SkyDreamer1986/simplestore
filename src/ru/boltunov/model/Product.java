package ru.boltunov.model;

import java.util.UUID;

public class Product {
	private UUID id;
	private String name;
	private Provider provider;
	private Integer itemCount;
	
	public Product() {
		super();
	}

	public Product(String name, Provider provider, Integer itemCount) {
		super();
		this.id = UUID.randomUUID();
		this.name = name;
		this.provider = provider;
		this.itemCount = itemCount;
	}
	
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
		
	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}
	
	public Integer getItemCount() {
		return itemCount;
	}
	public void setItemCount(Integer itemCount) {
		this.itemCount = itemCount;
	}
}