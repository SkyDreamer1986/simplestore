package ru.boltunov.view;

import java.util.Map;
import java.util.TreeMap;

import org.controlsfx.dialog.*;

import ru.boltunov.model.*;
import ru.boltunov.Main;
import javafx.fxml.*;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class ProductEditDialogController {

	@FXML
	private TextField productName;
	
	@FXML
	private ComboBox<Provider> provider;
	
	@FXML 
	private TextField amount; 
	 
	private Product product;
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
		// ������������� ���� 
		this.productName.setText(product.getName());
		this.amount.setText(product.getItemCount().toString());
		
		if (this.main != null) {
			this.provider.getSelectionModel().select(product.getProvider());
		}
	}
	
	private Main main;
	public Main getMain() {
		return main;
	}
	
	private boolean isAccepted = false;
	public boolean isAccepted() {
		return isAccepted;
	}

	public void setAccepted(boolean isAccepted) {
		this.isAccepted = isAccepted;
	}
		
	public TextField getProductName() {
		return productName;
	}

	public void setProductName(TextField productName) {
		this.productName = productName;
	}

	public ComboBox<Provider> getProvider() {
		return provider;
	}

	public void setProvider(ComboBox<Provider> provider) {
		this.provider = provider;
	}

	public TextField getAmount() {
		return amount;
	}

	public void setAmount(TextField amount) {
		this.amount = amount;
	}

	private Stage dialogStage;
	public Stage getDialogStage() {
		return dialogStage;
	}

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	public void setMain(Main main) {
		this.main = main;
			
		if (this.main != null) {
			this.provider.setItems(main.getProviderList());
			this.provider.setConverter(new ProviderConverter());
		}
	}
	
	// ������������ ��� ����������� ���������� �� �������
	enum ErrorStatuses { EVERYTHING_OK, FIELD_NOT_FILLED, WRONG_DATA }
	
	private Map<ErrorStatuses, String> errorMessages;
		
	{
		errorMessages = new TreeMap<ErrorStatuses, String>();
		errorMessages.put(ErrorStatuses.EVERYTHING_OK, "������ ���");
		errorMessages.put(ErrorStatuses.FIELD_NOT_FILLED, "���� <FieldName> ����������� � ����������!");
		errorMessages.put(ErrorStatuses.WRONG_DATA, "� ���� <FieldName> ������������ ��������!");
	}
	
	private boolean isInteger(String intCandidate) {
		return intCandidate.matches("\\d+$");
	}
	
	private boolean isWord(String wordCandidate) {
		return wordCandidate.matches("\\S+\\W+$");
	}
	
	private String getErrorMsgText(ErrorStatuses error, String fieldName) {
		StringBuilder messageTemplate = new StringBuilder(errorMessages.get(error));
		
		messageTemplate.replace(messageTemplate.indexOf("<FieldName>"), messageTemplate.indexOf("<FieldName>") + "<FieldName>".length(), fieldName);
		
		return messageTemplate.toString();
	}
	
	private ErrorStatuses doValidation(){
		ErrorStatuses result = ErrorStatuses.EVERYTHING_OK;
		String fieldName = "";
		
		if (productName.getText().equals("") ) {
			System.out.print("productName.getText() is null");
			result = ErrorStatuses.FIELD_NOT_FILLED;
			fieldName = "������������";
		}
		else
			if (isWord(productName.getText())) {
				if (provider.getSelectionModel().getSelectedItem() == null) {
					result = ErrorStatuses.FIELD_NOT_FILLED;
					fieldName = "���������";
				}
				else
					if (amount.getText().equals("")) {
						result = ErrorStatuses.FIELD_NOT_FILLED;
						fieldName = "����������";
					}
					else
						if (!isInteger(amount.getText())) {
							result = ErrorStatuses.WRONG_DATA;
							fieldName = "����������";
						}
			}
			else
			{
				result = ErrorStatuses.WRONG_DATA;
				fieldName = "������������";
			}
		
		if (result != ErrorStatuses.EVERYTHING_OK) {
			Dialogs.create()
	        .title("������!")
	        .masthead("!")
	        .message(getErrorMsgText(result, fieldName))
	        .showWarning();
		}
		return result;
	}
	
	@FXML
	public void initialize(){
		
	}

	public ProductEditDialogController() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public void cancelClicked() {
		this.dialogStage.close();
	}
		
	public void okClicked() {
		// ��������� ��������
		if (doValidation() == ErrorStatuses.EVERYTHING_OK) {
			// ��������� ���������
			this.product.setName(productName.getText());
			this.product.setProvider(provider.getSelectionModel().getSelectedItem());
			this.product.setItemCount(Integer.parseInt(amount.getText()));
			
			// ���������� ��������� � ��������� ���� ����
			this.isAccepted = true;
			this.dialogStage.close();
		}
	}
}