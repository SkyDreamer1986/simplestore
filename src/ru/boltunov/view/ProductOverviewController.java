package ru.boltunov.view;

import java.io.IOException;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import ru.boltunov.Main;
import ru.boltunov.model.*;

public class ProductOverviewController {
	@FXML
	private TableView<Product> productTable;
	@FXML
	private TableColumn<Product, String> nameColumn;
	@FXML
	private TableColumn<Product, String> providerColumn;
	@FXML
	private TableColumn<Product, Integer> itemCountColumn;
	
	@FXML
	private MenuItem exitMenuItem;
	
	@FXML
	private Button addButton;
	@FXML
	private Button editButton;
	@FXML
	private Button deleteButton;
	
	private Main mainApp;
	
	public Main getMainApp() {
		return mainApp;
	}

	public void setMainApp(Main mainApp) {
		this.mainApp = mainApp;
		productTable.setItems(mainApp.getProductList());
	}

	private final int DIALOG_MAX_HEIGHT = 200;
	private final int DIALOG_MAX_WIDTH = 370;
	
	@SuppressWarnings("unchecked")
	@FXML
	public void initialize(){
		// ������������ ���� ������� � ������� �������
		nameColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("name"));
		
		nameColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Product,String>>() {
			@Override
			public void handle(CellEditEvent<Product, String> event) {
				// TODO Auto-generated method stub
				System.out.print("Name has been updated");
			}
		});
		
		providerColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Product,String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(
					CellDataFeatures<Product, String> param) {
				// TODO Auto-generated method stub
				return new ReadOnlyObjectWrapper<String>(param.getValue().getProvider().getName());
			}
		});
				
		itemCountColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("itemCount"));
		productTable.getColumns().setAll(nameColumn, providerColumn, itemCountColumn);
	}
	
	public ProductOverviewController() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ProductOverviewController(Main main) {
		super();
		// TODO Auto-generated constructor stub
		
		this.mainApp = main;
	}
	
	public void exitApplication(){
		try {
			System.out.print("It works!");
			mainApp.getPrimaryStage().close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean openProductDialog(Product product, String windowCaption) {
		//System.out.print("It works!");
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("view/ProductEditDialog.fxml"));
		
		BorderPane page;
		try {
			page = (BorderPane)loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle(windowCaption);
			
			dialogStage.setHeight(DIALOG_MAX_HEIGHT);
			dialogStage.setHeight(DIALOG_MAX_WIDTH);
			
			dialogStage.setMaxHeight(DIALOG_MAX_HEIGHT);
			dialogStage.setMaxWidth(DIALOG_MAX_WIDTH);
			
			dialogStage.setMinHeight(DIALOG_MAX_HEIGHT);
			dialogStage.setMinWidth(DIALOG_MAX_WIDTH);
			
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(mainApp.getPrimaryStage());
			
			ProductEditDialogController controller = loader.getController();
			controller.setMain(mainApp);
			controller.setProduct(product);
						
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			
			controller.setDialogStage(dialogStage);
			
			dialogStage.showAndWait();
			
			return controller.isAccepted();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public void addProduct() {
		Product newProduct = new Product("", null, 0);
		
		if (openProductDialog(newProduct, "�������� �����")) {
			mainApp.getProductList().add(newProduct);
		}
	}
	
	public void editProduct() {
		int selIndex = this.productTable.getSelectionModel().getSelectedIndex();
		if (selIndex > -1) {
			if (openProductDialog(this.mainApp.getProductList().get(selIndex), "�������� ������")) {
				productTable.setItems(null); 
				productTable.layout(); 
				productTable.setItems(mainApp.getProductList());
			}
		}
	}
	
	public void deleteProduct() {
		int selIndex = this.productTable.getSelectionModel().getSelectedIndex();
		if (selIndex > -1)
			this.mainApp.getProductList().remove(selIndex);
	}
}