package ru.boltunov;
	
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import ru.boltunov.model.Product;
import ru.boltunov.model.Provider;
import ru.boltunov.view.ProductOverviewController;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;

import org.sqlite.*;

public class Main extends Application {
	
	public static final int MAIN_WINDOW_INIT_HEIGHT = 600;
	public static final int MAIN_WINDOW_INIT_WIDTH = 800;
	public static final String MAIN_WINDOW_CAPTION = "������ ���������� �� JavaFX";
	
	private Stage primaryStage;
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	private BorderPane rootLayout;
	
	private ObservableList<Product> productList;
			
	public ObservableList<Product> getProductList() {
		return this.productList;
	}
	
	public void setProductList(ObservableList<Product> productList) {
		this.productList = productList;
	}
	
	private ObservableList<Provider> providerList;
	
	public ObservableList<Provider> getProviderList() {
		return providerList;
	}

	public void setProviderList(ObservableList<Provider> providerList) {
		this.providerList = providerList;
	}

	private void initRootLayout(){
		try {
			rootLayout = (BorderPane)FXMLLoader.load(getClass().getResource("view/Sample.fxml"));
			Scene scene = new Scene(rootLayout, MAIN_WINDOW_INIT_WIDTH, MAIN_WINDOW_INIT_HEIGHT);
			scene.getStylesheets().add(getClass().getResource("view/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private void showProductOverview(){
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/ProductOverview.fxml"));
			
			BorderPane productOverview = (BorderPane)loader.load();
			
			rootLayout.setCenter(productOverview);
			System.out.println(this);
			ProductOverviewController controller = loader.getController();
			controller.setMainApp(Main.this);
			
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle(MAIN_WINDOW_CAPTION);
		
		productList = FXCollections.observableArrayList();
		providerList = FXCollections.observableArrayList();
		
		// ������ ��� ������� ��������� ������� �����
		// ��� ������ ��������� ��, ����� ��������� ������ ������
		productList.add(new Product("����� ������������", null, 50));
		productList.add(new Product("������� ����������", null, 50));
		productList.add(new Product("�������� ������", null, 50));
		productList.add(new Product("������ �������", null, 50));
				
		providerList.add(new Provider("��� �������", "�. ��������, ��. ���������, 22", "777-777"));
		providerList.add(new Provider("��� ����", "�. ��������, ��. ���������, 22", "888-888"));
		
		productList.get(0).setProvider(providerList.get(0));
		productList.get(1).setProvider(providerList.get(1));
		productList.get(2).setProvider(providerList.get(1));
		productList.get(3).setProvider(providerList.get(0));
		
		Connection connection;
				
		String insertProviderStatement = "insert into 'Provider'(id, name, address, phone) values (?, ?, ?, ?) ";
		String selectProviderStatement = "select * from 'Provider'";
		
		// ������� �������� ���������� � ������� SQLlite � �������������� jdbc
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:database/SimpleStore.s3db");
			
			if (connection != null) {
				System.out.println("���������� �����������!");
				/*
				PreparedStatement insertStatement = connection.prepareStatement(insertProviderStatement);
				
				for (Provider p : providerList) {
					
					insertStatement.setString(1, p.getId().toString());
					insertStatement.setString(2, p.getName() );
					insertStatement.setString(3, p.getAddress());
					insertStatement.setString(4, p.getPhone());
					insertStatement.executeUpdate();	
				}
				System.out.println("������ ��������!");
				connection.commit();
				*/
				// � ������ ��������� ������ �� ��
				Statement selectStatement = connection.createStatement();
				ResultSet providerResultSet =  selectStatement.executeQuery(selectProviderStatement);
				
				Integer i = 0;
				while (providerResultSet.next()) {
					String id = providerResultSet.getString("id"); 
					String providerName = providerResultSet.getString("name");
					String providerAddress = providerResultSet.getString("address");
					String providerPhone = providerResultSet.getString("phone");
					
					System.out.println("Provider data #" + i.toString() + ":");
					System.out.println("id" + id + ":");
					System.out.println("name" + providerName + ":");
					System.out.println("address" + providerAddress + ":");
					System.out.println("phone" + providerPhone + ":");
				}
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		initRootLayout();
		showProductOverview();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
